(in-package :chaos-game)

(defun affine-transform-point (point transformation)
  (destructuring-bind ((a b e) (c d f)) transformation
    (let* ((x (x point))
           (y (y point))
           (new-x (+ (* x a) (* y b)))
           (new-y (+ (* x c) (* y d))))
      (point (+ new-x e) (+ new-y f)))))

(defun affine-transformer (transformation)
  (lambda (system)
    (let* ((last-point (first (points system))))
      (affine-transform-point last-point transformation))))

(defun affine-transformation (a b c d e f)
  (list
   (list a b e)
   (list c d f)))

(defun point (x y)
  (cons x y))

(defun x (point)
  (car point))

(defun y (point)
  (cdr point))

(defun transform (p1 p2)
  (destructuring-bind ((x1 . y1) (x2 . y2)) (list p1 p2)
    (point (+ x1 x2) (+ y1 y2))))

(defun scale (point factor)
  (destructuring-bind (x . y) point
    (point (* x factor) (* y factor))))

(defun rotate-point (angle point)
  (let ((x (x point))
        (y (y point))
        (sin (sin angle))
        (cos (cos angle)))
    (point (- (* x cos) (* y sin))
           (+ (* x sin) (* y cos)))))

(defun new-origin (origin point-to-transform)
  (transform (scale origin -1) point-to-transform))

(defun center-by-factor (p1 p2 factor)
  (let* ((new-origin-point (new-origin p1 p2))
         (new-point-scaled (scale new-origin-point factor))
         (center-by-factor (transform p1 new-point-scaled)))
    center-by-factor))

(defun system-point-centerer (point)
  (lambda (system)
    (let* ((last-point (first (points system)))
           (factor (factor system)))
      (center-by-factor point last-point factor))))

(defun equilateral-n-polygon (n &optional
                                  (start-point (point 0d0 0d0))
                                  (first-vector (point 1d0 0d0)))
  (loop with angle = (/ pi n 1/2)
        repeat n
        for current-point = start-point then (transform current-point rotate-vector)
        for rotate-vector = first-vector then (rotate-point angle rotate-vector)
        collecting current-point))

(defun polygon-to-functions (polygon)
  (mapcar #'system-point-centerer polygon))

(defun equilateral-triangle-vertices ()
  (list '(0d0 . 0d0)
        '(1d0 . 0d0)
        '(0.5d0 . 1d0)))

(defun unit-square-vertices ()
  '((0d0 . 0d0)
    (0d0 . 1d0)
    (1d0 . 0d0)
    (1d0 . 1d0)))

(defun empty-fit-box ()
  '((0d0 . 0d0)
    (0d0 . 0d0)))

(defun distance (p1 p2)
  (destructuring-bind ((x1 . y1) (x2 . y2)) (list p1 p2)
    (sqrt (+ (expt (- x1 x2) 2) (expt (- y1 y2) 2)))))

(defun fit-point-left-below-point (fit-point point)
  (destructuring-bind ((fit-x . fit-y) (x . y)) (list fit-point point)
    (when (< x fit-x)
      (setf fit-x x))
    (when (< y fit-y)
      (setf fit-y y))
    (cons fit-x fit-y)))

(defun fit-point-right-above-point (fit-point point)
  (destructuring-bind ((fit-x . fit-y) (x . y)) (list fit-point point)
    (when (> x fit-x)
      (setf fit-x x))
    (when (> y fit-y)
      (setf fit-y y))
    (cons fit-x fit-y)))

(defun fit-point-in-fit-box (fit-box point)
  (destructuring-bind (fit-below-left fit-above-right) fit-box
    (list
     (fit-point-left-below-point fit-below-left point)
     (fit-point-right-above-point fit-above-right point))))

(defun fit-box-width (fit-box)
  (let ((x1 (x (first fit-box)))
        (x2 (x (second fit-box))))
    (abs (- x1 x2))))

(defun fit-box-height (fit-box)
  (let ((y1 (y (first fit-box)))
        (y2 (y (second fit-box))))
    (abs (- y1 y2))))

(defun fit-box-height-relative-to-width (fit-box)
  (let ((width (fit-box-width fit-box))
        (height (fit-box-height fit-box)))
    (/ height width)))

(defun point-relative-in-fit-box (fit-box point)
  (let* ((width (fit-box-width fit-box))
         (height (fit-box-height fit-box))
         (transformed-point (new-origin (first fit-box) point))
         (transformed-x (x transformed-point))
         (transformed-y (y transformed-point))
         (scaled-x (/ transformed-x width))
         (scaled-y (/ transformed-y height)))
    (cons scaled-x scaled-y)))

(defun fit-box-scaler (fit-box)
  (lambda (point)
    (point-relative-in-fit-box fit-box point)))

(defclass chaos-system ()
  ((transformations :accessor transformations
                    :initarg :transformations
                    :initform (polygon-to-functions (equilateral-n-polygon 3)))
   (points :accessor points
           :initarg :points
           :initform (list (point 0d0 0d0)))
   (factor :accessor factor
           :initarg :factor
           :initform 0.5d0)
   (fit-box :accessor fit-box
            :initarg :fit-box
            :initform (empty-fit-box))
   (weights :accessor weights
            :initarg :weights
            :initform nil)))

(defun choose-transformation (system)
  (with-slots (transformations weights) system
    (if weights
        (choose-with-weights transformations weights)
        (choose transformations))))

(defun choose (list)
  (let* ((size (length list))
         (chosen (random size)))
    (nth chosen list)))

(defun cdf (weights)
  (loop with total = (reduce #'+ weights)
        for weight in weights
        summing weight into cumulative-sum
        collecting (/ cumulative-sum total)))

(defun bisect (list val)
  (loop with lo = 0
        and hi = (length list)
        for mid = (floor (+ lo hi) 2)
        while (< lo hi)
        do (if (< val (nth mid list))
               (setf hi mid)
               (setf lo (1+ mid)))
        finally (return lo)))

(defun choose-with-weights (list weights)
  (let ((cdf (cdf weights))
        (random (random 1d0)))
    (nth (bisect cdf random) list)))

(defun new-system-fit-box (system)
  (loop for point in (points system)
        and new-fit-box = (list (point 0d0 0d0) (point 0d0 0d0))
              then (fit-point-in-fit-box new-fit-box point)
        finally (return new-fit-box)))

(defun fit-points-in-chaos-box (system)
  (with-slots (points fit-box) system
    (loop for point in points
          and new-fit-box = fit-box then (fit-point-in-fit-box fit-box point)
          finally (setf fit-box new-fit-box))))

(defun fit-point-in-chaos-box (system new-point)
  (with-slots (fit-box) system
    (setf fit-box (fit-point-in-fit-box fit-box new-point))))

(defun generate-new-point (system)
  (funcall (choose-transformation system) system))

(defun add-new-point (system)
  (let ((new-point (generate-new-point system)))
    (push new-point (points system))
    (fit-point-in-chaos-box system new-point)))

(defun add-n-new-points (system n)
  (loop repeat n
        do (add-new-point system)))

(defun scale-position (size scale)
  (let ((new-position (round (* size scale))))
    (cond
      ((< new-position 1) 1)
      ((>= new-position size) (1- size))
      (t new-position))))

(defun enable-point-by-scale (image x y)
  (when (and (<= 0 x 1)
              (<= 0 y 1))
    (let* ((height (image-height image))
           (width (image-width image))
           (x-position (scale-position width x))
           (y-position (- height (scale-position height y))))
      (setf (aref image y-position x-position 0) 255))))

(defun nice-points (system &optional (fit-box (fit-box system)) (ignore-last 8))
  (let* ((points (points system))
         (system-size (length points))
         (last-index (- system-size ignore-last 1)))
    (mapcar (fit-box-scaler fit-box) (subseq points 0 last-index))))

(defun system->png-image (system size &optional (fit-box (fit-box system)))
  (let* ((width size)
         (height (round (* size (fit-box-height-relative-to-width fit-box)))))
    (loop with image = (make-image height width 1)
          for (x . y) in (nice-points system fit-box)
          do (enable-point-by-scale image x y)
          finally (return image))))

(defun write-system (system size file &optional (fit-box (fit-box system)))
  (let ((image (system->png-image system size fit-box)))
    (with-open-file (output file :direction :output :element-type '(unsigned-byte 8)
                                 :if-exists :supersede)
      (encode image output))))
